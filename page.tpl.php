<?php
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
    <head>
        <title><?php print $head_title; ?></title>
        <?php print $head; ?>
        <?php print $styles; ?>
        <!--[if IE 7]>
          <link rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie7-fixes.css" type="text/css">
        <![endif]-->
        <!--[if lte IE 6]>
          <link rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie6-fixes.css" type="text/css">
        <![endif]-->
        <?php print $scripts; ?>
    </head>
<body>
    <div id="wrapper">
        <div id="header-logo">
            <?php
              // Prepare header
              if ($logo || $site_title) {
                print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">';
                if ($logo) {
                  print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
                }
                print check_plain($site_name) . '</a></h1>';
              }
            ?>
            <p><em><?php print check_plain($site_slogan); ?></em></p>
        </div>
        <hr />
        <!-- end #logo -->
        <div id="header">
            <div id="menu">
                <?php if (isset($primary_links)) : ?>
                  <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
                <?php endif; ?>
            </div>
            <!-- end #menu -->
            <?php if ($search_box): ?>
            <div id="search_box">
                <?php print $search_box ?>
            </div>
            <?php endif; ?>
            <!-- end #search -->
        </div>
        <!-- end #header -->
        <!-- end #header-wrapper -->
        <div id="page">
        <div id="page-bgtop">
            <div id="content">
                <?php if ($show_messages && $messages): print $messages; endif; ?>
                <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
                <?php if ($tabs): print ''. $tabs .''; endif; ?>
                <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
                <?php print $content ?>
            </div>
            <!-- end #content -->
            <?php if ($left): ?>
            <div id="sidebar">
              <?php print $left; ?>
            </div><!-- /sidebar-first -->
            <?php endif; ?>
            <!-- end #sidebar -->
            <div style="clear: both;">&nbsp;</div>
        </div>
        </div>
        <!-- end #page -->
        <div id="footer">
            <p><?php print $footer_message . $footer; ?></p>
        </div>
        <!-- end #footer -->
    </div>
    <?php print $closure; ?>
</body>
</html>
