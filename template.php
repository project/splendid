<?php

function splendid_node_submitted($object) {
    return t('@b<br><span>Posted by !a</span>', array('!a' => theme('username', $object), '@b' => format_date($object->created, 'custom', 'l, F d, Y g:i A')));
}